latex input:	mmd-article-header  
Title:	Introducción a Markdown + Git  
Base Header Level:	1  
LaTeX Mode:	article  
latex input:	mmd-article-begin-doc  
latex footer:	mmd-article-footer  
keywords:	markdown,git,software
language:	spanish

# Markdown + GIT [inicio] 

## ¿Por qué?


Para trabajar centrados de otra forma:

* Escritura centrada en el contenido y no en el formato.

* Evitar ser esclavos del WYSIWYG (**W**hat **Y**ou **S**ee **I**s **W**hat **Y**ou **G**et): son los editores con los que siempre estás luchando para que el contenido se muestre como quieres, pero al final lo muestra como tu no quieres.

* Permitir el trabajo colaborativo de forma productiva.

	- Control de cambios
	- Control de conflictos

* Facilitar la conversión de formatos de documentos

### Otras Opciones? ###

Si pero esta es la mejor para *[nerds](http://en.wikipedia.org/wiki/Nerd)* :-)

* Ms Word + directorios compartido + multiples versiones de ficheros: 
* Google Docs:
	* Curva de aprendizaje menor
	* Sigue siendo WYSIWYG
	* No permite integrarlo con otros flujos de trabajo


# Markdown

## Qué es _**Markdown**_

* Un formato de fichero: *.md*
* Un software para convertir a HTML (y otros formatos.)

Usado habitualmente para:

- Generar ficheros "Readme"
- Escribir en blogs y foros
- Posibilidad de generar fichero de **texto enriquecido** a partir de texto plano.
	
Parecido a el formato Wiki pero:

- Simplificado
- Muchas implementaciones
- Muy usado

## Por qué usarlo:

Markdown:

* es sencillo
* es rápido
* es multiplataforma
* es flexible
* se adapta a cualquier flujo de trabajo

No sólo es una editor más o menos avanzado:

+ Es una nueva forma rápida de expresar ideas
+ Ayuda a reflejar mapas mentales en texto.
+ Permite centrarse en el contenido y no en el formato
+ Convertible a otros formatos
	+ [Convertible a LaTEX](http://savethevowels.org/essays/markdowndissertation.html): -> integración con el flujo de trabajo TEX:
		- [Pandoc](http://johnmacfarlane.net/pandoc/): 

				pandoc -f markdown --latex-engine=xelatex -R -i Readme.md -o Readme.tex
			
		- [mmd](http://fletcherpenney.net/multimarkdown/download/): 

				multimarkdown -t latex Readme.md > Readme.tex```

		- "Manualmente": con un script en perl, python, shell, etc


	+ Convertible a .DOC y .ODT
		+ Convertir Markdown a .doc es fácil
		+ Convertir .doc a Markdown es *ligeramente divertido al principio pero sobre todo, doloroso*


## Sintaxis

Muy **simple**. Para ver un resumen de las sintaxis en law web [pulsar aquí](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet):

* Para hacerlo más sencillo, las aplicaciones suelen incluir la sintáxis como opción en los menus.
* Un _Syntaxhighligthing_ ayuda también (es casi fundamental)


### Énfasis ###

	*italica*   **negrita**
	_italica_   __negrita__
	***negrita y cursiva***
	_**negrita y cursiva**_ 
	__*negrita y cursiva*__

*italica*   **negrita** 

_italica_   __negrita__ 

***negrita y cursiva***
_**negrita y cursiva**_ 
 __*negrita y cursiva*__

### Cabeceras ###

Estilo *Setext* (más visual):

	Header 1
	========

	Header 2
	--------

Estilo *atx-style* (más faciles de restructurar) (las marcas # son opcionales):

	# Header 1 #

	## Header 2 ##

	###### Header 6


### Listas ###

**Listas ordenadas**, con párrafos:

	1.  Foo
	3.  Bar

Se visualiza como:

1.  Foo
3.  Bar

(Markdown autonumera y corrige errores en las listas ordenadas)

**Listas no ordenadas**, con párrafos:

	*   Un elemento de una lista

		Con múltiples párrafos.

	*   Otro elemento

Se visualiza como:

*   Un elemento de una lista

	Con múltiples párrafos.

*   Otro elemento


Las listas se pueden anidar:

	*   Abacus
		* answer
	*   Bubbles
		1.  bunk
		2.  bupkis
			* BELITTLER
		3. burper
	*   Cunning

Se visualiza como:

*   Abacus
	* answer
*   Bubbles
	1.  bunk
	2.  bupkis
* BELITTLER
	3. burper
*   Cunning

Para dar más variedad visual, se pueden emplear también los símbolos **+** y **-** en vez de *

	* Un elemento de la lista
		+ Otro elemento de la lista
			- El tercer elemento de la lista

* Un elemento de la lista
	+ Otro elemento de la lista
		- El tercer elemento de la lista

#### **Observaciones sobre las listas **

* ¡La posición es importante! Una tabulación inicial convierte el texto en cita.

### Enlaces ###

"En línea":

	Un [ejemplo](http://url.com/ "Texto alternativo")

Se visualiza como:

Un [ejemplo](http://url.com/ "Texto alternativo")

Etiquetas como referencia (los textos alternativos son opcionales):

	Un [Ejemplo][ejemploid]. Después, en cualquier 
	otro lugar del documento se define:

	  [ejemploid]: http://example.com/  "Texto alternativo"

Se visualiza como:

Un [Ejemplo][ejemploid]. Después, en cualquier 
otro lugar del documento se define:

  [ejemploid]: http://example.com/  "Texto alternativo"
	  
### Imágenes ###

**En línea** (los títulos son opcionales):

	![Texto imagen](img/logo_ja.png "Texto alternativo")

Se visualiza como:

![Texto imagen](img/logo_ja.png "Texto alternativo")

**Como referencia**:

	![Texto imagen][imagenid]

	_Esto es un ejemplo. Debe aparecer una imagen arriba_.

	[imagenid]: img/logo_ja.png "Texto alternativo"

Se visualiza como

![Texto imágen][imagenid]

_Esto es un ejemplo. Debe aparecer una imagen arriba_.

[imagenid]: img/logo_ja.png "Texto alternativo"


### Citas ###

El texto:

	Esto es una línea normal
	> Esto es parte de un bloque de cita.
	> Esto es parte del mismo bloque de cita.
	>
	> > Esto es otro bloque de cita anidado.
	> > Esto es parte del bloque anidado.
	>
	> * Se pueden citar listas.
	> * Etc.

Se visualiza como:

Esto es una línea normal
> Esto es parte de un bloque de cita.
> Esto es parte del mismo bloque de cita.
>
> > Esto es otro bloque de cita anidado.
> > Esto es parte del bloque anidado.
>
> * Se pueden citar listas.
> * Etc.


### Código ###

	El `<codigo>` es delimitado
	por acentos abiertos.

	Pueden incluir acentos abiertos 
	de esta  `` `forma` ``.

El `<codigo>`  es delimitado
por acentos abiertos.

Pueden incluir acentos abiertos 
de esta  `` `forma` ``.

### Bloques de código ###

Se obtiene aplicando una sangría a cada línea de un bloque por 4 espacios o 1 tabulador para que un párrafo *normal" como esto :

	Esto es un párrafo normal
	
		Esto es un bloque de código
		
Se visualize de esta forma:

Esto es un párrafo normal

	Esto es un bloque de código

### Bloques de código  cerradas###

En lugar de sangra el texto, podemos obtener el mismo resultado cerrando el texto con un tripe acento abierto ```

	```bash
	for i in a b ;
	do
	echo $i ;  
	done
	```

Se visualiza como:

```bash
for i in a b ;
do
echo $i ;  
done
```


### Reglas horizontales ###

Tres o más asteriscos:

	---

	* * *

	- - - -


---

### Código Html ###

También se puede incluir directamente código HTML:
 
	<table>
	    <tr>
	        <td>Tabla ejemplo</td>
	    </tr>
	</table>

Se visualiza: 

<table>
    <tr>
        <td>Tabla ejemplo</td>
    </tr>
</table>
 


## Implementaciones

* Son muchas y no existe ninguna que puede considerarse estándar.
* Cada una añade nuevas funcionalidades.
* A destacar:
	- [Multimarkdown](http://fletcherpenney.net/multimarkdown/)
	- [GFM](https://help.github.com/articles/github-flavored-markdown/) (GitHub de Markdown)

### Multimarkdown

Es una mejorara a Markdown. A continuación indicamos algunas de sus funcionalidades.

#### Tablas

Una tablas así:

	| Col 	|     Es      | Coste  |
	|----:	|:-----------:|-------:|
	|   1 	| izquierda   | €1600  |
	|   2 	|  centrada   |   €12  |
	|   3 	|  derecha    |    €1  |

Se visualiza como

| Col	| Es	| Coste	|  
|----:	|:-----------:	|-------:	|  
| 1	| izquierda	| €1600	|  
| 2	| centrada	| €12	|  
| 3	| derecha	| €1	|  

Para más generar tablas más complejas podemos usar herramientas como [Tables_Generator](http://www.tablesgenerator.com/markdown_tables#).

#### Notas Pié De Página ####

Las notas a pie de página se escriben así:

	Esto es un texto con nota al pie [^nota1]
	
	[^nota1]: Esto es una nota al pie de página.

Esto es un texto con nota al pie [^nota1]

[^nota1]: Esto es una nota al pie de página.


#### Referencias cruzadas ####

Una gran solución cuando se escriben documentos extensos (sobre todo para el que después se lo tiene que leer): podemos referenciar contenido con un sólo click.

Por ejemplo, si hemos creado previavmente una sección con esta referencia `[inicio]`.

	# Markdown + GIT [inicio] #

para  crear un código que nos referencie a esta sección usamo:

	Volver a la [Primera sección]][inicio]

Y este código se visualizará así:

Volver a la [Primera sección][inicio]


#### Ecuaciones

Unas ecuaciones como estas:

	\\[ {x}_{1,2}=\frac{-b\pm \sqrt{{b}^{2}-4ac}}{2a} \\]
	\\[ {e}^{i\pi }+1=0 \\]
Se visualiza como:

\\[ {x}_{1,2}=\frac{-b\pm \sqrt{{b}^{2}-4ac}}{2a} \\]

\\[ {e}^{i\pi }+1=0 \\]


La sintaxis de las ecuaciones es la misma que la utilizada en las ecuacioens de LaTEX[^latex].

[^latex]: Para los no iniciados existen herramientas WYSYWYG para crear ecuaciones en Latex [Daum Equation Editor](https://chrome.google.com/webstore/detail/daum-equation-editor/dinfmiceliiomokeofbocegmacmagjhe)

## Definiciones ##

Se puede incluir definiciones de una forma elegante de la siguinete forma:

	Apple
	
	:   Pomaceous fruit of plants of the genus Malus in  the family Rosaceae.
	    Also the makers of really great products.

Que se visualiza como:

Apple

:   Pomaceous fruit of plants of the genus Malus in  the family Rosaceae.
    Also the makers of really great products.

## Extensiones ##

### [Critic Markup](Http://criticmarkup.com) ###


Complementa a Markdown para visualizar los cambios (necesita un editor adecuado):

* Adicción \{\+\+adicción\+\+\}

	{++adicción++}
	
* Borrado \{\-\-borrado\-\-\}

	{--borrado--}

* Substitution: \{\~\~a ~>b \~\~\}

	{~~a ~>b ~~}

* Comentario: \{\>\> Comentario \<\<\}

	{>> Comentario <<}

* Destacado : 	\{== Destacado ==}\{\>\>Comentario destacado <\<\}

	{== Destacado ==}\{>>Comentario destacado <<}


<div class="cm-highlighting">
    <p>Don't go around saying<span class="delete">{-- to people that--}</span> the world owes you a living. The world owes you nothing. It was here first. <span class="sub">{~~One~&gt;Only one~~}</span> thing is impossible for God: To find <span class="insert">{++any++}</span> sense in any copyright law on the planet. <span class="highlight">{==Truth is stranger than fiction==}</span><span class="comment">{&gt;&gt;strange but true&lt;&lt;}</span>, but it is because Fiction is obliged to stick to possibilities; Truth isn’t.</p>
</div>


## Herramientas

Para trabajar con Markdown:

* Sirve cualquier editor de texto. Si tiene estas cosas pues mejor:

	* Syntax highligting (*Coloreado de sintaxis*)
	* Ayudas a la sintaxis (*snippets*)
	* Previsualización
	* Exportación a otros formatos

* Atom [https://atom.io](https://atom.io)
	- Versiones para Windows, Mac y Linux
	- Múltiples paquetes y temas opcionales

* Otras
	- Web
		+ [markable.in](http://markable.in)
	- Escritorio
		+ [Remarkable](http://remarkableapp.net): Linux
		+ [Retext](http://sourceforge.net/projects/retext/): Linux
	 	+ [Multimarkdown](http://multimarkdown.com): OSX
		+ [iAwrite](http://www.iawriter.com/mac/): Integración OSX / iOS.
		+ [Marked 2](http://marked2app.com): Previsualizador, exportador y mucho más. Para OSX

# Git

Es un sistema distribuido de control de versiones

## ¿Qué es el control de versiones, y por qué debería importarte?

El control de versiones registra los cambios realizados sobre un archivo o conjunto de archivos, de modo que puedas recuperar versiones específicas más adelante.

Cualquier tipo de archivo que encuentres en un ordenador puede ponerse bajo control de versiones.

Pero **resulta más adecuado para fichero de texto** (como los fichero *.md* de **Markdown**) pues resulta más fácil distinguir los cambios. 

Ver [Acerca del control de versiones](http://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)

## ¿Por qué un control de versiones?

Un método de control de versiones usado por mucha gente es el siguiente :

* Copiar los archivos a otro directorio (quizás indicando la fecha y hora en que lo hicieron, si son avispados).
	- Este *enfoque* es muy común porque es muy simple, pero
	- También tremendamente propenso a errores. 
	- Es fácil olvidar en qué directorio te encuentras
	- Y guardar accidentalmente en el archivo equivocado
	- O sobre-escribir archivos que no querías.
	- Si se **trabaja en equipo** el control de cambios puede ser una pesadilla

Un sistema de control de versiones permite:

* Colaborar
* Guardar versiones (de un forma adecuada)
* Restaurar versiones anteriores
* Comprender qué ha sucedido
* Backup


## Qué es Git?

Git es un sistema de control de versiones distribuido (DCVS)

### CVS Local

* Los cambios se registran en una base de datos local:


![CVS local](img/cvs-local.png)

### CVS centralizado

* Los cambios se registran en un servidor centralizado.
* Los clientes dependen de este servidor.

![CVS centralizado](img/cvs-server.png)

### CVS Distribuido

* Cada cliente en local dispone de una copia de todo el repositorio de versiones 
*  Puede cambiarse de un lugar a otro fácilmente
*  Es posible la colaboración entre unos y otros

![CVS Distribuido](img/cvs-distributed.png)



## Dónde reside el repositorio GIT?

Es una CVS distribuida:

* En local
* En un servidor
	* Servidor propio (no tenemos)
	* Servidor en la nube:
		* **Github**
			- Muy popular
			- No permite repositorios privados de forma gratuita.
		* **Bitbucket**:
			- Permite repositorios privado de forma gratuita.

## Herramientas

Nota: Se incluye aquí (y no al final) para ayuda a comprender cómo se realizan operaciones sobre Git

* Lineas de comandos:

	git clone https://github.com/libgit2/libgit2

* Aplicaciones GUI: [http://git-scm.com/downloads/guis]()
	- Github:
		+ Github App:
			- [https://mac.github.com/]()
			- [https://windows.github.com/]()
		+ Atom
			Editor con integración github.
	- Git-cola:
		- Incluido en muchas distribuciones Linux
	- Bitbucket
		+ Sourcetree:
			- [http://www.sourcetreeapp.com]()


## Principios básicos Git:

### Nuevo repositorio

* Clonar uno existente
* Iniciar desde cero

### Grabar cambios

![Ciclo de vida](img/git-lifecycle.png)

* Incluir en el repositorio: **Tracked**
* Modificar: **Modified**
* Preparar para grabar el cambio: **Stage**
	* Todo
	* Algunos ficheros
	* Cambio dentro de un fichero
		* Cambio ("*Hunk*")
		* Líneas concreta.
* Grabar cambios: **Commit**
	* Se registran sólo los cambios que se encuentre *"Staged""*
	* Se puede incluir un comentario (muy recomendable)
	* Al hacer un *commit* se genera **versión**. 


### Revisar ###
Después de grabar cambios podemos:
* Consultar historia de cambios
* Diferencias entre versiones
	- Posible muy fácilmente en **ficheros de texto**
* Vuelta atrás (a una versión concreta)
* Etiquetas o **"tags"** para revisiones mayores
	* Identifican a **versiones **del documento
		* Los *commits* registran revisiones
		* Los *tag* registran grupo de cambios con un sentido (**versiones**)

### Trabajar de forma colaborativa

* El uso de Makdown + GIT facilita el trabajo colaborativo.
* Requiere servidor remoto:
	* *Publicamos* el repositorio en servidor o lo iniciamos directamente en él.
	* Asignamos:
		* Privacidad: Público o privado.
		* Asignamos permisos para usuarios
* Operaciones:
	- **Push**:  *Publicamos* el repositorio en servidor o lo iniciamos directamente en él.
	- **Pull**: bajamos los cambios que hayan realizado otros
		- Pull request: Solicitamos realizar un cambio a un repositorio público para el que no tengamos permiso:
			- Muy útil para grandes proyecto colaborativos.

* NO se producen errores sino **conflictos**, que se pueden resolver.
	- Sólo ocurren cuando se trabaja sobre la misma línea

### Más Opciones ###

* Branch
	- Merge
	- Rebase

## Más información

[http://git-scm.com/doc](http://git-scm.com/doc)

